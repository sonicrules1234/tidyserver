pub mod tidyup;
use std::io::{Read, Write};
use std::io::BufReader;
use std::io::BufRead;
//use std::io::BufWrite;
use std::io::BufWriter;

pub fn handle_connection_read(stream: &mut impl Read) -> String {
    let mut b = BufReader::new(stream);
    let mut length_string = String::new();
    b.read_line(&mut length_string).unwrap();
    let length = length_string.trim().parse::<u64>().unwrap();
    let mut c = b.take(length);
    let mut html_data = String::new();    
    c.read_to_string(&mut html_data).unwrap();
    //println!("{}", html_data);
    crate::tidyup::tidyup(html_data)
    
}

pub fn handle_connection_write(stream: &mut impl Write, html_data: String) {
    let length = html_data.len();
    //writeln!(stream, "{}", length).unwrap();
    //stream.write(&html_data.as_bytes()).unwrap();
    //stream.flush();
    let mut b = BufWriter::new(stream);
    let data = format!("{}\n{}", length, html_data);
    b.write_all(&data.as_bytes());
}
